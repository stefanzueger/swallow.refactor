﻿namespace Swallow.Refactor.Commands.RefactorSymbol;

using Refactor.Symbol;
using Testing.Commands;

public sealed class RefactorSymbolCommandTest : CommandTest<RefactorSymbolCommand, RefactorSymbolSettings>
{
    protected override RefactorSymbolCommand Command { get; } = new();

    [Test]
    public async Task FindsSymbolUnderCursor()
    {
        AddDocument("File.cs", """
            namespace SomeFile;

            public sealed record SomeRecord(int Id, string Name);
            """);

        await RunCommand(new() { Cursor = "File.cs;3:25" });
    }

    [Test]
    public async Task FindsSymbolViaType()
    {
        AddDocument("File.cs", """
                               namespace SomeFile;

                               public sealed record SomeRecord(int Id, string Name);
                               """);

        await RunCommand(new() { Type = "SomeRecord" });
    }

    [Test]
    public async Task FindsSymbolViaTypeAndMember()
    {
        AddDocument("File.cs", """
                               namespace SomeFile;

                               public sealed record SomeRecord(int Id, string Name);
                               """);

        await RunCommand(new() { Type = "SomeRecord", Member = "Name" });
    }
}
