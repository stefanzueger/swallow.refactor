﻿namespace Swallow.Refactor.Core;

using System.Reflection;
using Abstractions;
using Abstractions.Rewriting;

internal sealed class RegistryTest
{
    private static IRegistry Registry => ReflectionRegistry.CreateFrom(Assembly.GetExecutingAssembly());

    [Test]
    public void CanProduceParameterlessRewriter()
    {
        var rewriter = Registry.Rewriter.Create(nameof(ParameterlessRewriter));
        Assert.That(actual: rewriter, expression: Is.InstanceOf<ParameterlessRewriter>());
    }

    [Test]
    public void CanProducteRewriterWithStringParameters()
    {
        var rewriter = Registry.Rewriter.Create(name: nameof(StringParameterRewriter), "first", "second");
        Assert.That(actual: rewriter, expression: Is.InstanceOf<StringParameterRewriter>());
        var typedRewriter = rewriter as StringParameterRewriter;
        Assert.That(actual: typedRewriter?.First, expression: Is.EqualTo("first"));
        Assert.That(actual: typedRewriter?.Second, expression: Is.EqualTo("second"));
    }

    [Test]
    public void CanParseParametersWhoseTypeHasAStaticParseMethod()
    {
        var rewriter = Registry.Rewriter.Create(name: nameof(ParseableTypeParameterRewriter), "11");
        Assert.That(actual: rewriter, expression: Is.InstanceOf<ParseableTypeParameterRewriter>());
        var typedRewriter = rewriter as ParseableTypeParameterRewriter;
        Assert.That(actual: typedRewriter?.Number, expression: Is.EqualTo(11));
    }

    [Test]
    public void CanParseParametersWhichAreUsedForParamsArray()
    {
        var rewriter = Registry.Rewriter.Create(name: nameof(ParamsParameterRewriter), "a", "b", "c");
        Assert.That(actual: rewriter, expression: Is.InstanceOf<ParamsParameterRewriter>());
        var typedRewriter = rewriter as ParamsParameterRewriter;
        Assert.That(actual: typedRewriter?.Parameters, expression: Is.EqualTo(new[] { "a", "b", "c" }));
    }

    [Test]
    public void CanParseEmptyParametersForParamsArray()
    {
        var rewriter = Registry.Rewriter.Create(name: nameof(ParamsParameterRewriter));
        Assert.That(actual: rewriter, expression: Is.InstanceOf<ParamsParameterRewriter>());
        var typedRewriter = rewriter as ParamsParameterRewriter;
        Assert.That(actual: typedRewriter?.Parameters, expression: Is.Empty);
    }

    [Test]
    public void CanListAllFoundRewriters()
    {
        var rewriters = Registry.Rewriter.List();
        Assert.That(
            actual: rewriters,
            expression: Contains.Item(new SlimRewriterInfo(Name: nameof(ParameterlessRewriter), Parameters: Array.Empty<string>()))
                .Using<IRewriterInfo>(IsEquivalent));

        Assert.That(
            actual: rewriters,
            expression: Contains.Item(new SlimRewriterInfo(Name: nameof(StringParameterRewriter), Parameters: new[] { "first", "second" }))
                .Using<IRewriterInfo>(IsEquivalent));

        Assert.That(
            actual: rewriters,
            expression: Contains.Item(new SlimRewriterInfo(Name: nameof(ParseableTypeParameterRewriter), Parameters: new[] { "number" }))
                .Using<IRewriterInfo>(IsEquivalent));
    }

    [Test]
    public void CanProduceParameterlessTargetedRewriter()
    {
        var rewriter = Registry.TargetedRewriter.Create(nameof(TargetedRewriter));
        Assert.That(actual: rewriter, expression: Is.InstanceOf<TargetedRewriter>());
    }

    private static int IsEquivalent(IRewriterInfo left, IRewriterInfo right)
    {
        var areEqual = left.Name == right.Name
                       && left.Parameters.Count == right.Parameters.Count
                       && left.Parameters.Zip(right.Parameters).All(t => t.First.Name == t.Second.Name);

        return areEqual ? 0 : int.MaxValue;
    }

    private sealed record SlimRewriterInfo(string Name, string[] Parameters) : IRewriterInfo
    {
        public string? Description => null;
        IReadOnlyCollection<IRewriterParameterInfo> IRewriterInfo.Parameters => Parameters.Select(p => new SlimRewriterParameterInfo(p)).ToArray();
    }

    private sealed record SlimRewriterParameterInfo(string Name) : IRewriterParameterInfo
    {
        public string? Description => null;
    }
}

public class ParameterlessRewriter : IRewriter
{
    public Task RunAsync(DocumentEditor documentEditor, SyntaxTree syntaxTree)
    {
        throw new NotImplementedException();
    }
}

public class StringParameterRewriter : IRewriter
{
    public string First { get; }
    public string Second { get; }

    public StringParameterRewriter(string first, string second)
    {
        First = first;
        Second = second;
    }

    public Task RunAsync(DocumentEditor documentEditor, SyntaxTree syntaxTree)
    {
        throw new NotImplementedException();
    }
}

public class ParseableTypeParameterRewriter : IRewriter
{
    public int Number { get; }

    public ParseableTypeParameterRewriter(int number)
    {
        Number = number;
    }

    public Task RunAsync(DocumentEditor documentEditor, SyntaxTree syntaxTree)
    {
        throw new NotImplementedException();
    }
}

public class ParamsParameterRewriter : IRewriter
{
    public string[] Parameters { get; }

    public ParamsParameterRewriter(params string[] parameters)
    {
        Parameters = parameters;
    }

    public Task RunAsync(DocumentEditor documentEditor, SyntaxTree syntaxTree)
    {
        throw new NotImplementedException();
    }
}

public class TargetedRewriter : ITargetedRewriter
{
    public Task RunAsync(SolutionEditor solutionEditor, ISymbol target)
    {
        throw new NotImplementedException();
    }
}
